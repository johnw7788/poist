﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Utilities.Data;



namespace PoistAPForm
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public static string ConnectionString = CommonAccess.GetConnectionString("SQLConnectionString");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblResult.Text = "";
            }
        }

        protected void btnEnterInfo_Click(object sender, EventArgs e)
        {
            try
            {
                InsertNewARRecord(Convert.ToDateTime(txtDateAP.Text), Convert.ToDecimal(txtAR.Text), Convert.ToDecimal(txtAP.Text), Convert.ToDecimal(txtCOH.Text),
                 Convert.ToDecimal(txtLOC.Text), Convert.ToDecimal(txtDollars.Text));
                lblResult.Text = "Record has been entered!";
                lblResult.ForeColor = System.Drawing.Color.Black;
            }
            catch
            {
                lblResult.Text = "ERROR! No record was entered!";
                lblResult.ForeColor = System.Drawing.Color.Red;
            }           
        }

        public static void InsertNewARRecord(DateTime date, decimal AR, decimal AP, decimal COH, decimal LOC, decimal dollars)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("SQLConnectionString", "Make sure 'SQLConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["InsertNewARRecord"]))
                throw new ArgumentNullException("InsertNewARRecord", "Make sure 'InsertNewARRecord' appSetting is provided and set in the application's configuration file");

            try
            {
                SqlParameter[] sqlParams = new SqlParameter[6];
                sqlParams[0] = new SqlParameter("@recordDate", date);
                sqlParams[1] = new SqlParameter("@AR", AR);
                sqlParams[2] = new SqlParameter("@AP", AP);
                sqlParams[3] = new SqlParameter("@COH", COH);
                sqlParams[4] = new SqlParameter("@LOC", LOC);
                sqlParams[5] = new SqlParameter("@dollars", dollars);

                CommonAccess.ExecuteNonQuery(ConnectionString, ConfigurationManager.AppSettings["InsertNewARRecord"].ToString(), sqlParams);

                //}
                //public static string GetLastCallDateTime()
                //{
                //    string strlastCall = null;

                //    if (string.IsNullOrEmpty(ConnectionString))
                //        throw new ArgumentNullException("SQLConnectionString", "Make sure 'SQLConnectionString' connectionstring is set in the application's configuration file");

                //    if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["GetLastCallTime"]))
                //        throw new ArgumentNullException("GetLastCallTime", "Make sure 'GetLastCallTime' appSetting is provided and set in the application's configuration file");


                //    DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["GetLastCallTime"].ToString());

                //    if (dt != null && dt.Rows.Count > 0)
                //    {
                //        strlastCall = dt.Rows[0].GetString("DateCalled").Substring(0, 19);
                //    }
                //    return strlastCall;
                //}

            }
            catch //e as Exception
            {

            }
        }
    }
}