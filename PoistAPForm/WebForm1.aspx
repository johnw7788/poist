﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="PoistAPForm.WebForm1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 173px;
        }
        .auto-style2 {
            width: 27%;
        }
         </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div>
            <table width="400px" border="0" cellpadding="0" cellspacing="0">
                <tr>
                     <td align="center" style="font-family:Arial;font-size:24pt;">
                         <asp:Label runat="server">Financial Numbers Entry</asp:Label>
                    </td>
                </tr>
            </table>            
        </div>
        <div>
            <table width="400px" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" style="font-family:Arial;">
                        <asp:Label runat="server">Financial Values Entry Form</asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table class="auto-style2">
                <tr>
                    <td align="left" class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Date:"></asp:Label>                        
                    </td>
                    <td>
                        <asp:TextBox ID="txtDateAP" runat="server" Width="91px" AutoPostBack="true"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="Calendar1_PopupControlExtender" runat="server" TargetControlID ="txtDateAP" ></ajaxToolkit:CalendarExtender>
                    </td> 
                </tr>
                <%--<tr>
                    <td align="left" class="auto-style1">AR:</td>
                     <td>
                        <asp:TextBox ID="txtAR" runat="server" AutoPostBack="true"></asp:TextBox>
                     </td>
                </tr>--%>
                <tr>
                    <td align="left" class="auto-style1">AR:</td>
                     <td>
                        <asp:TextBox ID="txtAR" runat="server"></asp:TextBox>
                     </td>                                        
                </tr>
                <tr>
                    <td align="left" class="auto-style1">AP:</td>
                     <td>
                        <asp:TextBox ID="txtAP" runat="server"></asp:TextBox>
                     </td>
                </tr>
                <tr>
                    <td align="left" class="auto-style1">
                        Cash On Hand:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCOH" runat="server"></asp:TextBox>
                    </td>
                    </tr>
                    <tr>
                        <td align="left" class="auto-style1">
                            Line Of Credit Balance:&nbsp; 
                        </td>
                        <td>
                            <asp:TextBox ID="txtLOC" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="auto-style1">
                            Inventory: 
                        </td>
                        <td>
                            <asp:TextBox ID="txtDollars" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                    <td style="text-align:right" class="auto-style1" >
                        <asp:Button ID="btnEnterInfo" runat="server" Text="Submit Data" OnClick="btnEnterInfo_Click" />
                    </td>
                    <td style="text-align:left" class="auto-style1" >                        
                        <asp:Label ID="lblResult" runat="server" Font-Bold="True"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:SqlDataSource ID="dsEmployeeNbrs" runat="server" 
            ConnectionString="<%$ ConnectionStrings:SQLConnectionString %>" 
            SelectCommand="spGetEmployeeNbr" SelectCommandType="StoredProcedure">
        </asp:SqlDataSource>
    </form>
</body>
</html>

