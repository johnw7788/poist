﻿using System;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using RestSharp;
using Newtonsoft.Json;
using Utilities.Data;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NPOI.SS.Util;
using Utilities.Extensions;

namespace PoistCalls
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public static string ConnectionString = CommonAccess.GetConnectionString("SQLConnectionString");
        public static int i = 0;
        public static int iTot = 0;
        
        string callId;
        string userId;
        DateTime dateCalled;
        string path;
        string ownerType;
        string ownerId;
        string ownerName;
        int extension;
        string siteName;
        string callType;
        int callerNumberType;
        string calledFrom;
        int calleeNumberType;
        string callerDIDNumber;
        string calleeDIDNumber;
        string callerName;
        string calledTo;
        string direction;
        int duration;
        string result;

        protected void Page_Load(object sender, EventArgs e)
        {
            GetData();
        }

        private void GetData()
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            txtResult.Text += "Getting Latest DateTime ";
            try
            {                
                DateTime now = DateTime.Now;
                var callLast = Convert.ToDateTime(GetLastCallDateTime());
                callLast = callLast.AddSeconds(1);
                var callUpTo = callLast.AddHours(24);
                NPOI.SS.Util.DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                string callFrom = df.Format(callLast);
                string callTo = df.Format(callUpTo);

                txtResult.Text += "Last datetime " + callFrom + " ";

                var apiSecret = ConfigurationManager.AppSettings["apiSecret"]; //"hhjDw9SSxF0DoPslcjyhaprUTJ9kBYKRbWwW";
                byte[] symmetricKey = Encoding.ASCII.GetBytes(apiSecret);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = ConfigurationManager.AppSettings["issuer"], //"cvp62lZPTymiSuKY3-no2w",
                    Expires = now.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256),
                };

                var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                var xml_text = "";
                dynamic DynamicData = "";
                txtResult.Text += "Going to connect ";

                //var curToken = "";
                var request = new RestRequest(Method.GET);
                var client = new RestClient("https://api.zoom.us/v2/phone/call_logs?page_size=300&from=" + callFrom + "&to=" + callTo + ""); // &next_page_token=" + curToken + "'");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", String.Format("Bearer {0}", tokenString));

                IRestResponse response = client.Execute(request);
                txtResult.Text += "Got a response ";
                xml_text = response.Content;
                DynamicData = JsonConvert.DeserializeObject(xml_text);
                //curToken = DynamicData.next_page_token;
                txtResult.Text += " " + DynamicData.total_records + " ";
                if (DynamicData.total_records != null)
                {
                    iTot = DynamicData.total_records;
                    txtResult.Text += "iTot = " + iTot + " ";
                }                

                while (i < iTot)
                {
                    callId = DynamicData.call_logs[i].id;
                    userId = DynamicData.call_logs[i].user_id;
                    dateCalled = DynamicData.call_logs[i].date_time;
                    path = DynamicData.call_logs[i].path;
                    ownerType = DynamicData.call_logs[i].owner.type;
                    ownerId = DynamicData.call_logs[i].owner.id;
                    ownerName = DynamicData.call_logs[i].owner.name;
                    extension = DynamicData.call_logs[i].owner.extension_number;
                    siteName = DynamicData.call_logs[i].site.name;
                    callType = DynamicData.call_logs[i].call_type;
                    callerNumberType = DynamicData.call_logs[i].caller_number_type;
                    calledFrom = DynamicData.call_logs[i].callee_number;
                    calleeNumberType = DynamicData.call_logs[i].callee_number_type;
                    callerDIDNumber = DynamicData.call_logs[i].caller_did_number;
                    calleeDIDNumber = DynamicData.call_logs[i].callee_did_number;
                    callerName = DynamicData.call_logs[i].caller_name;
                    calledTo = DynamicData.call_logs[i].callee_name;
                    direction = DynamicData.call_logs[i].direction;
                    duration = DynamicData.call_logs[i].duration;
                    result = DynamicData.call_logs[i].result;
                    UpdateDeliveryDateTable(callId, userId, dateCalled, path, ownerType, ownerId, ownerName, extension, siteName, callType,
                        callerNumberType, calledFrom, calleeNumberType, callerDIDNumber, calleeDIDNumber, callerName, calledTo,
                        direction, duration, result);
                    i += 1;
                }
                txtResult.Text += "Records updated ";
            }                
            catch (Exception e)
            {
                txtResult.Text += System.Environment.NewLine + e.ToString();

            }
            txtResult.Text += "Program ending ";
        }

        public static string GetLastCallDateTime()
        {
            string strlastCall = null;

            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("SQLConnectionString", "Make sure 'SQLConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["GetLastCallTime"]))
                throw new ArgumentNullException("GetLastCallTime", "Make sure 'GetLastCallTime' appSetting is provided and set in the application's configuration file");


            DataTable dt = CommonAccess.GetData(ConnectionString, ConfigurationManager.AppSettings["GetLastCallTime"].ToString());

            if (dt != null && dt.Rows.Count > 0)
            {
                strlastCall = dt.Rows[0].GetString("DateCalled").Substring(0,19);
            }
            return strlastCall;
        }

        public static void UpdateDeliveryDateTable(string callId, string userId, DateTime dateCalled, string path, string @ownerType, string @ownerID,
            string @ownerName, int @extension, string @siteName, string @callType,int @callerNumberType, string @calledFrom, int @calleeNumberType,
            string @callerDIDNumber, string @calleeDIDNumber, string @callerName, string @callTo, string @direction, int @duration, string @result)
        {            

            if (string.IsNullOrEmpty(ConnectionString))
                throw new ArgumentNullException("SQLConnectionString", "Make sure 'SQLConnectionString' connectionstring is set in the application's configuration file");

            if (string.IsNullOrEmpty(ConfigurationManager.AppSettings["InsertCallRecords"]))
                throw new ArgumentNullException("InsertCallRecords", "Make sure 'InsertCallRecords' appSetting is provided and set in the application's configuration file");

            SqlParameter[] sqlParams = new SqlParameter[20];
            sqlParams[0] = new SqlParameter("@callId", callId);
            sqlParams[1] = new SqlParameter("@userId", userId);
            sqlParams[2] = new SqlParameter("@dateCalled", dateCalled);
            sqlParams[3] = new SqlParameter("@path", path);
            sqlParams[4] = new SqlParameter("@ownerType", ownerType);
            sqlParams[5] = new SqlParameter("@ownerID", ownerID);
            sqlParams[6] = new SqlParameter("@ownerName", ownerName);
            sqlParams[7] = new SqlParameter("@extension", extension);
            sqlParams[8] = new SqlParameter("@siteName", siteName);
            sqlParams[9] = new SqlParameter("@callType", callType);
            sqlParams[10] = new SqlParameter("@callerNumberType", callerNumberType);
            sqlParams[11] = new SqlParameter("@calledFrom", calledFrom);
            sqlParams[12] = new SqlParameter("@calleeNumberType", calleeNumberType);
            sqlParams[13] = new SqlParameter("@callerDIDNumber", callerDIDNumber);
            sqlParams[14] = new SqlParameter("@calleeDIDNumber", calleeDIDNumber);
            sqlParams[15] = new SqlParameter("@callerName", callerName);
            sqlParams[16] = new SqlParameter("@callTo", callTo);
            sqlParams[17] = new SqlParameter("@direction", direction);
            sqlParams[18] = new SqlParameter("@duration", duration);
            sqlParams[19] = new SqlParameter("@result", result);           
            CommonAccess.ExecuteNonQuery(ConnectionString, ConfigurationManager.AppSettings["InsertCallRecords"].ToString(), sqlParams);

        }
    }
}