﻿using System;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using RestSharp;
using Newtonsoft.Json;
using Utilities.Data;
using System.Data;
using System.Data.SqlClient;
using NPOI.SS.Util;
using Utilities.Extensions;

namespace PoistCallService
{
    class Program
    {
        public static int i = 0;
        public static int iTot = 0;

        static void Main(string[] args)
        {
            GetData();
        }
        private static void GetData()
        {  
            string callId;
            string userId;
            DateTime dateCalled;
            string path;
            string ownerType;
            string ownerId;
            string ownerName;
            int extension;
            string siteName;
            string callType;
            int callerNumberType;
            string calledFrom;
            int calleeNumberType;
            string callerDIDNumber;
            string calleeDIDNumber;
            string callerName;
            string calledTo;
            string direction;
            int duration;
            string result;

            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            //Console.WriteLine("Getting Latest DateTime");
            try
            {
                //Utilities.Logging.LogWriter.Write("Starting the service to get the latest calls.");
                DateTime now = DateTime.Now;
                var callLast = Convert.ToDateTime(GetLastCallDateTime());
                callLast = callLast.AddSeconds(1);
                var callUpTo = callLast.AddHours(24);
                NPOI.SS.Util.DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
                string callFrom = df.Format(callLast);
                string callTo = df.Format(callUpTo);

                //Console.WriteLine("Last datetime " + callFrom);

                var apiSecret = "hhjDw9SSxF0DoPslcjyhaprUTJ9kBYKRbWwW";
                byte[] symmetricKey = Encoding.ASCII.GetBytes(apiSecret);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = "cvp62lZPTymiSuKY3-no2w",
                    Expires = now.AddDays(1),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256),
                };
                
                var tokenHandler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);
                var tokenString = tokenHandler.WriteToken(token);
                WriteLogNote("CallLogs", "Token", tokenString, null, DateTime.Now);
                var xml_text = "";
                dynamic DynamicData = "";
                //Console.WriteLine("Going to connect");

                WriteLogNote("CallLogs", "Service", "Starting to get file", null, DateTime.Now);

                var request = new RestRequest(Method.GET);
                var client = new RestClient("https://api.zoom.us/v2/phone/call_logs?page_size=300&from=" + callFrom + "&to=" + callTo + ""); // &next_page_token=" + curToken + "'");
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", String.Format("Bearer {0}", tokenString));

                WriteLogNote("CallLogs", "Service", request.ToString(), null, DateTime.Now);
                WriteLogNote("CallLogs", "Service", "Convert to XML file", null, DateTime.Now);

                IRestResponse response = client.Execute(request);
                //Console.WriteLine("Got a response");
                xml_text = response.Content;
                WriteLogNote("CallLogs", "XML", xml_text, null, DateTime.Now);
                DynamicData = JsonConvert.DeserializeObject(xml_text);
                //Console.WriteLine("We have total records 0f -  " + DynamicData.total_records);
                if (DynamicData.total_records != null)
                {
                    iTot = DynamicData.total_records;
                    //Console.WriteLine("iTot = " + iTot);
                }
                WriteLogNote("CallLogs", "Service", "Reading records", null, DateTime.Now);

                while (i < iTot)
                {
                    callId = DynamicData.call_logs[i].id;
                    userId = DynamicData.call_logs[i].user_id;
                    dateCalled = DynamicData.call_logs[i].date_time;
                    path = DynamicData.call_logs[i].path;
                    ownerType = DynamicData.call_logs[i].owner.type;
                    ownerId = DynamicData.call_logs[i].owner.id;
                    ownerName = DynamicData.call_logs[i].owner.name;
                    extension = DynamicData.call_logs[i].owner.extension_number;
                    siteName = DynamicData.call_logs[i].site.name;
                    callType = DynamicData.call_logs[i].call_type;
                    callerNumberType = DynamicData.call_logs[i].caller_number_type;
                    calledFrom = DynamicData.call_logs[i].callee_number;
                    calleeNumberType = DynamicData.call_logs[i].callee_number_type;
                    callerDIDNumber = DynamicData.call_logs[i].caller_did_number;
                    calleeDIDNumber = DynamicData.call_logs[i].callee_did_number;
                    callerName = DynamicData.call_logs[i].caller_name;
                    calledTo = DynamicData.call_logs[i].callee_name;
                    direction = DynamicData.call_logs[i].direction;
                    duration = DynamicData.call_logs[i].duration;
                    result = DynamicData.call_logs[i].result;
                    UpdateDeliveryDateTable(callId, userId, dateCalled, path, ownerType, ownerId, ownerName, extension, siteName, callType,
                        callerNumberType, calledFrom, calleeNumberType, callerDIDNumber, calleeDIDNumber, callerName, calledTo,
                        direction, duration, result);
                    i += 1;
                }
                //Console.WriteLine(iTot + " Records updated");
                WriteLogNote("CallLogs", "Service", iTot.ToString() + " Records Import Complete", null, DateTime.Now);
            }
            catch (Exception e)
            {
                //Console.WriteLine(System.Environment.NewLine + e.ToString());
                WriteLogNote("CallLogs", "Service", "Records Import Error", e.ToString(), DateTime.Now);

            }
            //Console.WriteLine("Program ending");
        }

        public static string GetLastCallDateTime()
        {
            string strlastCall = null;
            var datasource = "POISTSVR";//your server
            var database = "PoistCalls"; //your database name
            var username = "cpAppUser"; //username of server to connect
            var password = "C0rp0rate P0rtal Acce$$"; //password

            //your connection string 
            string ConnectionString = @"Data Source=" + datasource + ";Initial Catalog="
                        + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnectionString);

            
            DataTable dt = CommonAccess.GetData(ConnectionString, "spGetLastCallDateTime");

            if (dt != null && dt.Rows.Count > 0)
            {
                strlastCall = dt.Rows[0].GetString("DateCalled").Substring(0, 19);
            }
            return strlastCall;
        }

        public static void UpdateDeliveryDateTable(string callId, string userId, DateTime dateCalled, string path, string @ownerType, string @ownerID,
            string @ownerName, int @extension, string @siteName, string @callType, int @callerNumberType, string @calledFrom, int @calleeNumberType,
            string @callerDIDNumber, string @calleeDIDNumber, string @callerName, string @callTo, string @direction, int @duration, string @result)
        {

            var datasource = "POISTSVR";//your server
            var database = "PoistCalls"; //your database name
            var username = "cpAppUser"; //username of server to connect
            var password = "C0rp0rate P0rtal Acce$$"; //password

            //your connection string 
            string ConnectionString = @"Data Source=" + datasource + ";Initial Catalog="
                        + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnectionString);            
           
            SqlParameter[] sqlParams = new SqlParameter[20];
            sqlParams[0] = new SqlParameter("@callId", callId);
            sqlParams[1] = new SqlParameter("@userId", userId);
            sqlParams[2] = new SqlParameter("@dateCalled", dateCalled);
            sqlParams[3] = new SqlParameter("@path", path);
            sqlParams[4] = new SqlParameter("@ownerType", ownerType);
            sqlParams[5] = new SqlParameter("@ownerID", ownerID);
            sqlParams[6] = new SqlParameter("@ownerName", ownerName);
            sqlParams[7] = new SqlParameter("@extension", extension);
            sqlParams[8] = new SqlParameter("@siteName", siteName);
            sqlParams[9] = new SqlParameter("@callType", callType);
            sqlParams[10] = new SqlParameter("@callerNumberType", callerNumberType);
            sqlParams[11] = new SqlParameter("@calledFrom", calledFrom);
            sqlParams[12] = new SqlParameter("@calleeNumberType", calleeNumberType);
            sqlParams[13] = new SqlParameter("@callerDIDNumber", callerDIDNumber);
            sqlParams[14] = new SqlParameter("@calleeDIDNumber", calleeDIDNumber);
            sqlParams[15] = new SqlParameter("@callerName", callerName);
            sqlParams[16] = new SqlParameter("@callTo", callTo);
            sqlParams[17] = new SqlParameter("@direction", direction);
            sqlParams[18] = new SqlParameter("@duration", duration);
            sqlParams[19] = new SqlParameter("@result", result);
            CommonAccess.ExecuteNonQuery(ConnectionString, "spInsertCallRecords", sqlParams);

        }

        public static void WriteLogNote(string appName, string userName, string message, string exception, DateTime LogDate)
        {
            var datasource = "POISTSVR";//your server
            var database = "PoistCalls"; //your database name
            var username = "cpAppUser"; //username of server to connect
            var password = "C0rp0rate P0rtal Acce$$"; //password

            //your connection string 
            string ConnectionString = @"Data Source=" + datasource + ";Initial Catalog="
                        + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;

            //create instanace of database connection
            SqlConnection conn = new SqlConnection(ConnectionString);

            SqlParameter[] sqlParams = new SqlParameter[5];
            sqlParams[0] = new SqlParameter("@appName", appName);
            sqlParams[1] = new SqlParameter("@userName", userName);
            sqlParams[2] = new SqlParameter("@message", message);
            sqlParams[3] = new SqlParameter("@exception", exception);
            sqlParams[4] = new SqlParameter("@logDate", LogDate);
            CommonAccess.ExecuteNonQuery(ConnectionString, "spInsertCallLog", sqlParams);
        }

    }
}
